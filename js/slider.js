document.querySelectorAll(".cards").forEach((item) => setSlider(item));

function setSlider(container) {
	let sliderPosition = 0;
	const sliderContainer = container.querySelector(".slider__wrapper");
	const cardsList = container.querySelector(".cards__list");
	const card = container.querySelectorAll(".card");
	const sliderButtonPrev = container.querySelector(".btn_prev");
	const sliderButtonNext = container.querySelector(".btn_next");

	const cardWidth = card[0].offsetWidth + 10;
	const sliderContainerWidth = sliderContainer.offsetWidth;
	const sliderTrackWidth = card.length * cardWidth - sliderContainerWidth - 11;

	sliderButtonPrev.addEventListener("click", function () {
		sliderPosition += cardWidth;
		if (sliderPosition > 0) {
			sliderPosition = 0;
		}
		cardsList.style.transform = `translateX(${sliderPosition}px)`;
		sliderButtons();
	});

	sliderButtonNext.addEventListener("click", function () {
		sliderPosition -= cardWidth;
		if (sliderPosition < -sliderTrackWidth) {
			sliderPosition = -sliderTrackWidth;
		}
		cardsList.style.transform = `translateX(${sliderPosition}px)`;
		sliderButtons();
	});

	const sliderButtons = () => {
		if (sliderPosition == 0) {
			sliderButtonPrev.classList.add("disable");
			sliderButtonPrev.disabled = true;
		} else {
			sliderButtonPrev.classList.remove("disable");
			sliderButtonPrev.disabled = false;
		}
		if (sliderPosition == -sliderTrackWidth) {
			sliderButtonNext.classList.add("disable");
			sliderButtonNext.disabled = true;
		} else {
			sliderButtonNext.classList.remove("disable");
			sliderButtonNext.disabled = false;
		}
	};
	sliderButtons();
}