let menuItems = document.querySelectorAll('.menu__item_hasSubmenu');
menuItems.forEach((item) => item.addEventListener('click', function(event) {
    event.preventDefault();
    item.classList.toggle('submenu_closed'); 
}));