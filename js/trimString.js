const basicParagraphs = document.querySelector('.cards__list').querySelectorAll('p');

basicParagraphs.forEach((item) => item.innerHTML = (item.innerHTML.length > 20) ? item.innerHTML.slice(0, 20) + '...' : item.innerHTML);